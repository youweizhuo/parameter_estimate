import numpy
import matplotlib.pyplot as plt

mean1 = [1, 1]
cov1 = [[12, 0], [0, 1]]
mean2 = [7, 7]
cov2 = [[8, 3], [3, 2]]
mean3 = [15, 1]
cov3 = [[2, 0], [0, 2]]
#numpy.random.multivariate_normal(mean1, cov1)
data_mean = [mean1, mean2, mean3]
data_cov = [cov1, cov2, cov3]

# sample iid
prob_prior = [0.6, 0.3, 0.1]
#prob_prior = [1.0/3, 1.0/3, 1.0/3]
num_sample = 3000
array_sample = []
for i in xrange(num_sample):
    draw = numpy.random.choice(range(3), p=prob_prior)
    sample = numpy.random.multivariate_normal(data_mean[draw], data_cov[draw])
    array_sample.append((draw, sample))

# MLE
sum_category = [[0, 0], [0, 0], [0, 0]]
cnt_category = [0, 0, 0]
for i in xrange(num_sample):
    category, sample = array_sample[i]
    sum_category[category] = numpy.add(sum_category[category], sample)
    #map(lambda (x, y): x + y, zip(sum_category[category], sample))
    cnt_category[category] += 1

est_mean = [numpy.divide(sum_category[i], cnt_category[i]) for i in xrange(3)]

est_cov = [[[0, 0], [0, 0]], [[0, 0], [0, 0]], [[0, 0], [0, 0]]]
for i in xrange(num_sample):
    category, sample = array_sample[i]
    vec_diff = numpy.subtract(sample, est_mean[category])
    #map(lambda (x, y): x - y, zip(sample, est_mean[category]))
    #print vec_diff
    mat_diff = numpy.multiply(vec_diff[:, None], vec_diff[None, :])
    #print mat_diff
    est_cov[category] = numpy.add(est_cov[category], mat_diff)

est_cov = [numpy.divide(est_cov[i], cnt_category[i]) for i in xrange(3)]
print est_cov
# BE
